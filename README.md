# Mes jolies TP de linux 🌼🌷


- [TP1 : Casser avant de construire](./TP_Linux/TP1)
- [TP2 : Appréhension d'un système Linux](./TP_Linux/TP2)
- [TP3 : Services](./TP_Linux/TP3)
- [TP4 : Real services](./TP_Linux/TP4)

