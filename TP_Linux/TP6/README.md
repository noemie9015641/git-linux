# TP6 : Self-hosted cloud

## I. Mise en place et maîtrise du serveur Web

### 1. Installation

🌞 Installer le serveur Apache
> sudo dnf install httpd -y
```powershell
[nono@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
[sudo] password for nono:
ServerRoot "/etc/httpd"

Listen 80
Include conf.modules.d/*.conf

User apache
Group apache
ServerAdmin root@localhost
<Directory />
    AllowOverride none
    Require all denied
</Directory>


DocumentRoot "/var/www/html"


<Directory "/var/www">
    AllowOverride None

    Require all granted
</Directory>

<Directory "/var/www/html">


    Options Indexes FollowSymLinks


    AllowOverride None
        Require all granted
</Directory>


<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

<Files ".ht*">
    Require all denied
</Files>

ErrorLog "logs/error_log"

LogLevel warn

<IfModule log_config_module>

    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>

      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>

    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>

    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>


<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>

    TypesConfig /etc/mime.types

    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz

    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

AddDefaultCharset UTF-8

<IfModule mime_magic_module>
        MIMEMagicFile conf/magic
</IfModule>


EnableSendfile on


IncludeOptional conf.d/*.conf
```

🌞 Démarrer le service Apache

``` powershell
[nono@web ~]$ sudo systemctl restart httpd
[nono@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; preset: disabled)
     Active: active (running) since Mon 2024-03-25 16:39:53 CET; 2s ago
       Docs: man:httpd.service(8)
   Main PID: 4633 (httpd)
     Status: "Started, listening on: port 80"
      Tasks: 213 (limit: 4668)
     Memory: 34.8M
        CPU: 55ms
     CGroup: /system.slice/httpd.service
             ├─4633 /usr/sbin/httpd -DFOREGROUND
             ├─4634 /usr/sbin/httpd -DFOREGROUND
             ├─4635 /usr/sbin/httpd -DFOREGROUND
             ├─4636 /usr/sbin/httpd -DFOREGROUND
             └─4637 /usr/sbin/httpd -DFOREGROUND

Mar 25 16:39:53 web.tp6.linux systemd[1]: Starting The Apache HTTP Server...
Mar 25 16:39:53 web.tp6.linux systemd[1]: Started The Apache HTTP Server.
Mar 25 16:39:53 web.tp6.linux httpd[4633]: Server configured, listening on: port 80
```

> sudo systemctl enable httpd

```powershell
[nono@web ~]$ sudo ss -alnpt | grep httpd
[sudo] password for nono:
LISTEN 0      511                *:80              *:*    users:(("httpd",pid=727,fd=4),("httpd",pid=726,fd=4),("httpd",pid=725,fd=4),("httpd",pid=704,fd=4))
```

🌞 TEST

> sudo systemctl status httpd

```powershell
 Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; preset: disabled)
 Active: active (running) since Mon 2024-03-25 19:07:59 CET; 1h 7min ago
```

```powershell
[nono@web ~]$ curl localhost
<!doctype html>
<html>
  <body>
    <h1>HTTP Server <strong>Test Page</strong></h1>

    <div class='row'>

      <div class='col-sm-12 col-md-6 col-md-6 '></div>
          <p class="summary">This page is used to test the proper operation of
            an HTTP server after it has been installed on a Rocky Linux system.
            If you can read this page, it means that the software is working
            correctly.</p>
      </div>

      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>


        <div class='section'>
          <h2>Just visiting?</h2>

          <p>This website you are visiting is either experiencing problems or
          could be going through maintenance.</p>

          <p>If you would like the let the administrators of this website know
          that you've seen this page instead of the page you've expected, you
          should send them an email. In general, mail sent to the name
          "webmaster" and directed to the website's domain should reach the
          appropriate person.</p>

          <p>The most common email address to send to is:
          <strong>"webmaster@example.com"</strong></p>

          <h2>Note:</h2>
          <p>The Rocky Linux distribution is a stable and reproduceable platform
          based on the sources of Red Hat Enterprise Linux (RHEL). With this in
          mind, please understand that:

        <ul>
          <li>Neither the <strong>Rocky Linux Project</strong> nor the
          <strong>Rocky Enterprise Software Foundation</strong> have anything to
          do with this website or its content.</li>
          <li>The Rocky Linux Project nor the <strong>RESF</strong> have
          "hacked" this webserver: This test page is included with the
          distribution.</li>
        </ul>
        <p>For more information about Rocky Linux, please visit the
          <a href="https://rockylinux.org/"><strong>Rocky Linux
          website</strong></a>.
        </p>
        </div>
      </div>
      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>
        <div class='section'>

          <h2>I am the admin, what do I do?</h2>

        <p>You may now add content to the webroot directory for your
        software.</p>

        <p><strong>For systems using the
        <a href="https://httpd.apache.org/">Apache Webserver</strong></a>:
        You can add content to the directory <code>/var/www/html/</code>.
        Until you do so, people visiting your website will see this page. If
        you would like this page to not be shown, follow the instructions in:
        <code>/etc/httpd/conf.d/welcome.conf</code>.</p>

        <p><strong>For systems using
        <a href="https://nginx.org">Nginx</strong></a>:
        You can add your content in a location of your
        choice and edit the <code>root</code> configuration directive
        in <code>/etc/nginx/nginx.conf</code>.</p>

        <div id="logos">
          <a href="https://rockylinux.org/" id="rocky-poweredby"><img src="icons/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->
          <img src="poweredby.png" /> <!-- webserver -->
        </div>
      </div>
      </div>

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```


```powershell
[nono@web ~]$ curl http://10.6.1.11
<!doctype html>
<html>
  <body>
    <h1>HTTP Server <strong>Test Page</strong></h1>

    <div class='row'>

      <div class='col-sm-12 col-md-6 col-md-6 '></div>
          <p class="summary">This page is used to test the proper operation of
            an HTTP server after it has been installed on a Rocky Linux system.
            If you can read this page, it means that the software is working
            correctly.</p>
      </div>

      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>


        <div class='section'>
          <h2>Just visiting?</h2>

          <p>This website you are visiting is either experiencing problems or
          could be going through maintenance.</p>

          <p>If you would like the let the administrators of this website know
          that you've seen this page instead of the page you've expected, you
          should send them an email. In general, mail sent to the name
          "webmaster" and directed to the website's domain should reach the
          appropriate person.</p>

          <p>The most common email address to send to is:
          <strong>"webmaster@example.com"</strong></p>

          <h2>Note:</h2>
          <p>The Rocky Linux distribution is a stable and reproduceable platform
          based on the sources of Red Hat Enterprise Linux (RHEL). With this in
          mind, please understand that:

        <ul>
          <li>Neither the <strong>Rocky Linux Project</strong> nor the
          <strong>Rocky Enterprise Software Foundation</strong> have anything to
          do with this website or its content.</li>
          <li>The Rocky Linux Project nor the <strong>RESF</strong> have
          "hacked" this webserver: This test page is included with the
          distribution.</li>
        </ul>
        <p>For more information about Rocky Linux, please visit the
          <a href="https://rockylinux.org/"><strong>Rocky Linux
          website</strong></a>.
        </p>
        </div>
      </div>
      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>
        <div class='section'>

          <h2>I am the admin, what do I do?</h2>

        <p>You may now add content to the webroot directory for your
        software.</p>

        <p><strong>For systems using the
        <a href="https://httpd.apache.org/">Apache Webserver</strong></a>:
        You can add content to the directory <code>/var/www/html/</code>.
        Until you do so, people visiting your website will see this page. If
        you would like this page to not be shown, follow the instructions in:
        <code>/etc/httpd/conf.d/welcome.conf</code>.</p>

        <p><strong>For systems using
        <a href="https://nginx.org">Nginx</strong></a>:
        You can add your content in a location of your
        choice and edit the <code>root</code> configuration directive
        in <code>/etc/nginx/nginx.conf</code>.</p>

        <div id="logos">
          <a href="https://rockylinux.org/" id="rocky-poweredby"><img src="icons/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->
          <img src="poweredby.png" /> <!-- webserver -->
        </div>
      </div>
      </div>

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```

### 2. Avancer vers la maîtrise du service

🌞 Le service Apache...

```powershell
[nono@web ~]$ sudo cat /usr/lib/systemd/system/httpd.service
[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```

🌞 Déterminer sous quel utilisateur tourne le processus Apache

> sudo cat /etc/httpd/conf/httpd.conf

```powershell
User apache
Group apache
```

```powershell
[nono@web conf]$ ps -ef | grep apache
apache      1516    1515  0 09:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1517    1515  0 09:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1518    1515  0 09:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1519    1515  0 09:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
nono        1778    1480  0 09:36 pts/0    00:00:00 grep --color=auto apache
```

```powershell
[nono@web testpage]$ ls -al
total 12
drwxr-xr-x.  2 root root   24 Mar 25 16:19 .
drwxr-xr-x. 83 root root 4096 Mar 25 16:19 ..
-rw-r--r--.  1 root root 7620 Feb 21 14:12 index.html
```

🌞 Changer l'utilisateur utilisé par Apache

> sudo useradd -d /usr/share/httpd -s /sbin/nologin toto

```powershell
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
toto:x:1001:1001::/usr/share/httpd:/sbin/nologin
```

```powershell
[nono@web conf]$ sudo cat httpd.conf | grep User
User toto
```

>sudo systemctl restart httpd

```powershell
[nono@web conf]$ ps -ef | grep toto
toto        1850    1849  0 10:05 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
toto        1851    1849  0 10:05 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
toto        1852    1849  0 10:05 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
toto        1853    1849  0 10:05 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
nono        2067    1480  0 10:05 pts/0    00:00:00 grep --color=auto toto
```

🌞 Faites en sorte que Apache tourne sur un autre port


```powershell
[nono@web conf]$ sudo cat httpd.conf | grep Listen
Listen 180
```

```powershell
[nono@web conf]$ sudo firewall-cmd --add-port=180/tcp --permanent
success
[nono@web conf]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[nono@web conf]$ sudo firewall-cmd --reload
success
```

> sudo systemctl restart httpd

> sudo ss -alnpt

```powershell
LISTEN     0          511                        *:180                      *:*         users:(("httpd",pid=2099,fd=4),("httpd",pid=2098,fd=4),("httpd",pid=2097,fd=4),("httpd",pid=2094,fd=4))
```

```powershell
[nono@web conf]$ curl localhost:180
<!doctype html>
<html>
  <body>
    <h1>HTTP Server <strong>Test Page</strong></h1>

    <div class='row'>

      <div class='col-sm-12 col-md-6 col-md-6 '></div>
          <p class="summary">This page is used to test the proper operation of
            an HTTP server after it has been installed on a Rocky Linux system.
            If you can read this page, it means that the software is working
            correctly.</p>
      </div>

      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>


        <div class='section'>
          <h2>Just visiting?</h2>

          <p>This website you are visiting is either experiencing problems or
          could be going through maintenance.</p>

          <p>If you would like the let the administrators of this website know
          that you've seen this page instead of the page you've expected, you
          should send them an email. In general, mail sent to the name
          "webmaster" and directed to the website's domain should reach the
          appropriate person.</p>

          <p>The most common email address to send to is:
          <strong>"webmaster@example.com"</strong></p>

          <h2>Note:</h2>
          <p>The Rocky Linux distribution is a stable and reproduceable platform
          based on the sources of Red Hat Enterprise Linux (RHEL). With this in
          mind, please understand that:

        <ul>
          <li>Neither the <strong>Rocky Linux Project</strong> nor the
          <strong>Rocky Enterprise Software Foundation</strong> have anything to
          do with this website or its content.</li>
          <li>The Rocky Linux Project nor the <strong>RESF</strong> have
          "hacked" this webserver: This test page is included with the
          distribution.</li>
        </ul>
        <p>For more information about Rocky Linux, please visit the
          <a href="https://rockylinux.org/"><strong>Rocky Linux
          website</strong></a>.
        </p>
        </div>
      </div>
      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>
        <div class='section'>

          <h2>I am the admin, what do I do?</h2>

        <p>You may now add content to the webroot directory for your
        software.</p>

        <p><strong>For systems using the
        <a href="https://httpd.apache.org/">Apache Webserver</strong></a>:
        You can add content to the directory <code>/var/www/html/</code>.
        Until you do so, people visiting your website will see this page. If
        you would like this page to not be shown, follow the instructions in:
        <code>/etc/httpd/conf.d/welcome.conf</code>.</p>

        <p><strong>For systems using
        <a href="https://nginx.org">Nginx</strong></a>:
        You can add your content in a location of your
        choice and edit the <code>root</code> configuration directive
        in <code>/etc/nginx/nginx.conf</code>.</p>

        <div id="logos">
          <a href="https://rockylinux.org/" id="rocky-poweredby"><img src="icons/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->
          <img src="poweredby.png" /> <!-- webserver -->
        </div>
      </div>
      </div>

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```


```powershell
[nono@web conf]$ curl http://10.6.1.11:180
<!doctype html>
<html>
  <body>
    <h1>HTTP Server <strong>Test Page</strong></h1>

    <div class='row'>

      <div class='col-sm-12 col-md-6 col-md-6 '></div>
          <p class="summary">This page is used to test the proper operation of
            an HTTP server after it has been installed on a Rocky Linux system.
            If you can read this page, it means that the software is working
            correctly.</p>
      </div>

      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>


        <div class='section'>
          <h2>Just visiting?</h2>

          <p>This website you are visiting is either experiencing problems or
          could be going through maintenance.</p>

          <p>If you would like the let the administrators of this website know
          that you've seen this page instead of the page you've expected, you
          should send them an email. In general, mail sent to the name
          "webmaster" and directed to the website's domain should reach the
          appropriate person.</p>

          <p>The most common email address to send to is:
          <strong>"webmaster@example.com"</strong></p>

          <h2>Note:</h2>
          <p>The Rocky Linux distribution is a stable and reproduceable platform
          based on the sources of Red Hat Enterprise Linux (RHEL). With this in
          mind, please understand that:

        <ul>
          <li>Neither the <strong>Rocky Linux Project</strong> nor the
          <strong>Rocky Enterprise Software Foundation</strong> have anything to
          do with this website or its content.</li>
          <li>The Rocky Linux Project nor the <strong>RESF</strong> have
          "hacked" this webserver: This test page is included with the
          distribution.</li>
        </ul>
        <p>For more information about Rocky Linux, please visit the
          <a href="https://rockylinux.org/"><strong>Rocky Linux
          website</strong></a>.
        </p>
        </div>
      </div>
      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>
        <div class='section'>

          <h2>I am the admin, what do I do?</h2>

        <p>You may now add content to the webroot directory for your
        software.</p>

        <p><strong>For systems using the
        <a href="https://httpd.apache.org/">Apache Webserver</strong></a>:
        You can add content to the directory <code>/var/www/html/</code>.
        Until you do so, people visiting your website will see this page. If
        you would like this page to not be shown, follow the instructions in:
        <code>/etc/httpd/conf.d/welcome.conf</code>.</p>

        <p><strong>For systems using
        <a href="https://nginx.org">Nginx</strong></a>:
        You can add your content in a location of your
        choice and edit the <code>root</code> configuration directive
        in <code>/etc/nginx/nginx.conf</code>.</p>

        <div id="logos">
          <a href="https://rockylinux.org/" id="rocky-poweredby"><img src="icons/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->
          <img src="poweredby.png" /> <!-- webserver -->
        </div>
      </div>
      </div>

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```

## II. Mise en place et maîtrise du serveur de base de données

🌞 Install de MariaDB sur db.tp6.linux

> sudo dnf install mariadb-server
> sudo systemctl enable mariadb
> sudo systemctl start mariadb

🌞 Port utilisé par MariaDB

> sudo ss -alnpt | grep mariadb

```powershell
LISTEN     0          80                         *:3306                     *:*         users:(("mariadbd",pid=13312,fd=19))
```

> sudo firewall-cmd --add-port=3306/tcp --permanent
> sudo firewall-cmd --reload

🌞 Processus liés à MariaDB

```powershell
[nono@db ~]$ ps -ef | grep mariadb
mysql      13312       1  0 10:33 ?        00:00:00 /usr/libexec/mariadbd --basedir=/usr
nono       13425    1282  0 10:41 pts/0    00:00:00 grep --color=auto mariadb
```

## III. Configuration et mise en place de NextCloud

### 1. Base de données

🌞 Préparation de la base pour NextCloud

>sudo mysql -u root -p

> CREATE USER 'nextcloud'@'10.6.1.11' IDENTIFIED BY 'pewpewpew';
> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.6.1.11';
> FLUSH PRIVILEGES;

```powershell
[nono@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 7
Server version: 10.5.22-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.6.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (2.202 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.002 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.6.1.11';
Query OK, 0 rows affected (0.116 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

🌞 Exploration de la base de données

> mysql -u nextcloud -h 10.6.1.12 -p

```powershell
[nono@web ~]$ mysql -u nextcloud -h 10.6.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 5.5.5-10.5.22-MariaDB MariaDB Server

Copyright (c) 2000, 2024, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```

```powershell
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud;
Database changed
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> SHOW TABLES;
Empty set (0.01 sec)
```

🌞 Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données

On fait comme pour nextcloud on lui autorise la connexion a partir de web.tp6.linux

```powershell
MariaDB [(none)]> GRANT ALL PRIVILEGES ON *.* TO 'root'@'10.6.1.11' IDENTIFIED BY 'nono';
Query OK, 0 rows affected (0.119 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.004 sec)
```

> mysql -u root -h 10.6.1.12 -p

```
mysql> SELECT User FROM mysql.user;
+-------------+
| User        |
+-------------+
| nextcloud   |
| root        |
| mariadb.sys |
| mysql       |
| root        |
+-------------+
5 rows in set (0.01 sec)
```

## 2. Serveur Web et NextCloud

🌞 Install de PHP

> sudo dnf install php

```
[nono@web ~]$ php -v
PHP 8.0.30 (cli) (built: Aug  3 2023 17:13:08) ( NTS gcc x86_64 )
Copyright (c) The PHP Group
Zend Engine v4.0.30, Copyright (c) Zend Technologies
    with Zend OPcache v8.0.30, Copyright (c), by Zend Technologies
```

🌞 Récupérer NextCloud

> sudo mkdir /var/www/tp6_nextcloud

> sudo wget https://download.nextcloud.com/server/releases/latest.zip

> sudo unzip lastest.zip

> sudo chown apache /var/www/tp6_nextcloud/*

```powershell
[nono@web tp6_nextcloud]$ ls -al
total 8
drwxr-xr-x.  3 apache root   41 Mar 26 11:20 .
drwxr-xr-x.  5 root   root   54 Mar 26 11:15 ..
-rw-r--r--.  1 apache root  156 Feb 29 08:46 index.html
drwxr-xr-x. 13 apache root 4096 Mar 26 11:20 nextcloud
```

🌞 Adapter la configuration d'Apache

```powershell
[nono@web tp6_nextcloud]$ sudo cat /etc/httpd/conf/httpd.conf | grep Include
Include conf.modules.d/*.conf
IncludeOptional conf.d/*.conf
```

```powershell
[nono@web conf.d]$ sudo nano webroot.conf
[nono@web conf.d]$ ls
autoindex.conf  php.conf  README  userdir.conf  webroot.conf  welcome.conf
[nono@web conf.d]$ pwd
/etc/httpd/conf.d
```

🌞 Redémarrer le service Apache pour qu'il prenne en compte le nouveau fichier de conf

> sudo systemctl restart httpd

## 3. Finaliser l'installation de NextCloud

🌞 Installez les deux modules PHP dont NextCloud vous parle

> sudo dnf install php-zip 
> sudo dnf install php-gd

🌞 Pour que NextCloud utilise la base de données, ajoutez aussi

> sudo dnf install php-pdo
> sudo dnf install php-mysqlnd

🌞 Exploration de la base de données

```
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| MariaDB            |
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
+--------------------+
5 rows in set (0.007 sec)
MariaDB [(none)]> USE nextcloud;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [nextcloud]> SHOW TABLES;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
| oc_accounts_data            |
| oc_activity                 |
| oc_activity_mq              |
| oc_addressbookchanges       |
| oc_addressbooks             |
| oc_appconfig                |
| oc_authorized_groups        |
| oc_authtoken                |
| oc_bruteforce_attempts      |
| oc_calendar_invitations     |
| oc_calendar_reminders       |
| oc_calendar_resources       |
| oc_calendar_resources_md    |
| oc_calendar_rooms           |
| oc_calendar_rooms_md        |
| oc_calendarchanges          |
| oc_calendarobjects          |
| oc_calendarobjects_props    |
| oc_calendars                |
| oc_calendarsubscriptions    |
| oc_cards                    |
| oc_cards_properties         |
| oc_circles_circle           |
| oc_circles_event            |
| oc_circles_member           |
| oc_circles_membership       |
| oc_circles_mount            |
| oc_circles_mountpoint       |
| oc_circles_remote           |
| oc_circles_share_lock       |
| oc_circles_token            |
| oc_collres_accesscache      |
| oc_collres_collections      |
| oc_collres_resources        |
| oc_comments                 |
| oc_comments_read_markers    |
| oc_dav_absence              |
| oc_dav_cal_proxy            |
| oc_dav_shares               |
| oc_direct_edit              |
| oc_directlink               |
| oc_federated_reshares       |
| oc_file_locks               |
| oc_filecache                |
| oc_filecache_extended       |
| oc_files_metadata           |
| oc_files_metadata_index     |
| oc_files_reminders          |
| oc_files_trash              |
| oc_files_versions           |
| oc_flow_checks              |
| oc_flow_operations          |
| oc_flow_operations_scope    |
| oc_group_admin              |
| oc_group_user               |
| oc_groups                   |
| oc_jobs                     |
| oc_known_users              |
| oc_login_flow_v2            |
| oc_migrations               |
| oc_mimetypes                |
| oc_mounts                   |
| oc_notifications            |
| oc_notifications_pushhash   |
| oc_notifications_settings   |
| oc_oauth2_access_tokens     |
| oc_oauth2_clients           |
| oc_open_local_editor        |
| oc_photos_albums            |
| oc_photos_albums_collabs    |
| oc_photos_albums_files      |
| oc_preferences              |
| oc_privacy_admins           |
| oc_profile_config           |
| oc_properties               |
| oc_ratelimit_entries        |
| oc_reactions                |
| oc_recent_contact           |
| oc_schedulingobjects        |
| oc_share                    |
| oc_share_external           |
| oc_storages                 |
| oc_storages_credentials     |
| oc_systemtag                |
| oc_systemtag_group          |
| oc_systemtag_object_mapping |
| oc_text2image_tasks         |
| oc_text_documents           |
| oc_text_sessions            |
| oc_text_steps               |
| oc_textprocessing_tasks     |
| oc_trusted_servers          |
| oc_twofactor_backupcodes    |
| oc_twofactor_providers      |
| oc_user_status              |
| oc_user_transfer_owner      |
| oc_users                    |
| oc_vcategory                |
| oc_vcategory_to_object      |
| oc_webauthn                 |
| oc_whats_new                |
+-----------------------------+
102 rows in set (0.004 sec)
```

102 tables
