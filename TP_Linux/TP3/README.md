# TP3 : Services

## I. Service SSH

### 1. Analyse du service

🌞 S'assurer que le service sshd est démarré

```powershell
[nono@node1 ~]$ systemctl status
● node1.tp2.b1
    State: running
    Units: 283 loaded (incl. loaded aliases)
     Jobs: 0 queued
   Failed: 0 units
    Since: Mon 2024-01-29 10:43:02 CET; 2min 22s ago
  systemd: 252-18.el9
```

🌞 Analyser les processus liés au service SSH 

```powershell
[nono@node1 ~]$ ps -efl | grep ssh
4 S root         706       1  0  80   0 -  3963 -      10:42 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
4 S root        1258     706  0  80   0 -  4712 -      10:42 ?        00:00:00 sshd: nono    [priv]
5 S nono           1262    1258  0  80   0 -  4758 -      10:42 ?        00:00:00 sshd: nono@pts/0
0 S nono           1322    1263  0  80   0 -  1602 pipe_r 11:02 pts/0    00:00:00 grep --color=auto ssh
```

🌞 Déterminer le port sur lequel écoute le service SSH

le port 22
```powershell
[nono@node1 ~]$ sudo ss -alnpt | grep sshd
LISTEN 0      128          0.0.0.0:22        0.0.0.0:*    users:(("sshd",pid=706,fd=3))
LISTEN 0      128             [::]:22           [::]:*    users:(("sshd",pid=706,fd=4))
```

🌞 Consulter les logs du service SSH


> journalctl -xe -u sshd

```powershell
[nono@node1 log]$ sudo tail cat secure
tail: cannot open 'cat' for reading: No such file or directory
==> secure <==
Jan 29 11:24:24 node1 sudo[1382]: pam_unix(sudo:session): session closed for user root
Jan 29 11:24:31 node1 sudo[1385]:    nono    : TTY=pts/0 ; PWD=/var/log ; USER=root ; COMMAND=/bin/cat secure tal
Jan 29 11:24:31 node1 sudo[1385]: pam_unix(sudo:session): session opened for user root(uid=0) by nono   (uid=1000)
Jan 29 11:24:31 node1 sudo[1385]: pam_unix(sudo:session): session closed for user root
Jan 29 11:24:34 node1 sudo[1388]:    nono    : TTY=pts/0 ; PWD=/var/log ; USER=root ; COMMAND=/bin/cat secure tail
Jan 29 11:24:34 node1 sudo[1388]: pam_unix(sudo:session): session opened for user root(uid=0) by nono   (uid=1000)
Jan 29 11:24:34 node1 sudo[1388]: pam_unix(sudo:session): session closed for user root
Jan 29 11:24:22 node1 sudo[1391]:    nono    : TTY=pts/0 ; PWD=/var/log ; USER=root ; COMMAND=/bin/cat tail secure
Jan 29 11:24:22 node1 sudo[1391]: pam_unix(sudo:session): session opened for user root(uid=0) by nono   (uid=1000)
Jan 29 11:24:22 node1 sudo[1391]: pam_unix(sudo:session): session closed for user root
```

### 2. Modification du service

🌞 Identifier le fichier de configuration du serveur SSH

> /etc/ssh/sshd_config

🌞 Modifier le fichier de conf

```powershell
[nono@node1 ssh]$ echo $RANDOM
19761

[nono@node1 ~]$ sudo cat /etc/ssh/sshd_config | grep Port
Port 19761
#GatewayPorts no   

[nono@node1 ssh]$ sudo firewall-cmd --remove-port=22/tcp --permanent
Warning: NOT_ENABLED: 22:tcp
success
[nono@node1 ssh]$ sudo firewall-cmd --add-port=19761/tcp --permanent
success
[nono@node1 ssh]$ sudo firewall-cmd --reload
success

[nono@node1 ssh]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 19761/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 Redémarrer le service

```powershell
[nono@node1 ssh]$ sudo systemctl restart firewalld
```

🌞 Effectuer une connexion SSH sur le nouveau port
```powershell
PS C:\Users\mimis> ssh nono@10.2.1.11
ssh: connect to host 10.2.1.11 port 22: Connection refused
PS C:\Users\mimis> ssh -p 19761 nono@10.2.1.11
nono@10.2.1.11's password:
Last login: Mon Jan 29 11:44:24 2024
```

## II. Service HTTP

### 1. Mise en place

🌞 Installer le serveur NGINX

Déjà installé :
```powershell
[nono@node1 ~]$ sudo dnf install nginx
[sudo] password for nono   :
Last metadata expiration check: 1:20:15 ago on Mon 29 Jan 2024 10:34:07 AM CET.
Package nginx-1:1.20.1-14.el9_2.1.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

🌞 Démarrer le service NGINX

> sudo systemctl start nginx

```powershell
[nono@node1 ~]$ sudo systemctl start nginx
[nono@node1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; preset: disabled)
     Active: active (running) since Mon 2024-01-29 11:59:57 CET; 3s ago
    Process: 1309 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 1310 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 1311 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 1312 (nginx)
      Tasks: 2 (limit: 4591)
     Memory: 3.2M
        CPU: 128ms
     CGroup: /system.slice/nginx.service
             ├─1312 "nginx: master process /usr/sbin/nginx"
             └─1313 "nginx: worker process"
```
🌞 Déterminer sur quel port tourne NGINX

```powershell
[nono@node1 ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      511          0.0.0.0:80         0.0.0.0:*    users:(("nginx",pid=1313,fd=6),("nginx",pid=1312,fd=6))
LISTEN 0      511             [::]:80            [::]:*    users:(("nginx",pid=1313,fd=7),("nginx",pid=1312,fd=7))
[nono@node1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[nono@node1 ~]$ sudo firewall-cmd --reload
success
```

🌞 Déterminer les processus liés au service NGINX

```
[nono@node1 ~]$ ps -ef | grep nginx
root        1312       1  0 11:59 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1313    1312  0 11:59 ?        00:00:00 nginx: worker process
nono           1364    1257  1 12:09 pts/0    00:00:00 grep --color=auto nginx
```

🌞 Déterminer le nom de l'utilisateur qui lance NGINX

```
[nono@node1 ~]$ cat /etc/passwd | grep nginx
nginx:x:991:991:Nginx web server:/var/lib/nginx:/sbin/nologin
```

🌞 Test !
```
$ curl http://10.2.1.11:80 | head
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
100  7620  100  7620    0     0  1125k      0 --:--:-- --:--:-- --:--:-- 1240k
curl: Failed writing body
```

### 2. Analyser la conf de NGINX

🌞 Déterminer le path du fichier de configuration de NGINX

```powershell
[nono@node1 nginx]$ ls -al /etc/nginx/nginx.conf
-rw-r--r--. 1 root root 2334 Oct 16 20:01 /etc/nginx/nginx.conf
```

🌞 Trouver dans le fichier de conf

```powershell
[nono@node1 nginx]$ cat nginx.conf | grep server -A 10
    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }

------------------------------------

[nono@node1 nginx]$ cat nginx.conf | grep include
include /etc/nginx/conf.d/*.conf;
```

### 3. Déployer un nouveau site web

🌞 Créer un site web

```
[nono@node1 var]$ sudo mkdir www/tp3_linux
[nono@node1 var]$ cd www/tp3_linux/
[nono@node1 tp3_linux]$ sudo touch index.html
```
🌞 Gérer les permissions

```
[nono@node1 tp3_linux]$ sudo chown nginx:nginx /var/www/tp3_linux/index.html
[sudo] password for nono   :
[nono@node1 tp3_linux]$ ls -al
total 4
drwxr-xr-x. 2 root  root  24 Jan 30 10:19 .
drwxr-xr-x. 3 root  root  23 Jan 30 10:18 ..
-rw-r--r--. 1 nginx nginx 42 Jan 30 10:10 index.html
```

🌞 Adapter la conf NGINX

> /etc/nginx/conf.d

```powershell
[nono@node1 conf.d]$ touch server.conf
[nono@node1 conf.d]$ echo $RANDOM
26171
[nono@node1 conf.d]$ sudo nano server.conf
[nono@node1 conf.d]$ cat server.conf
server {
  listen 26171;

  root /var/www/tp3_linux;
}
[nono@node1 conf.d]$ sudo firewall-cmd --add-port=26171/tcp --permanent
success
[nono@node1 conf.d]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[nono@node1 conf.d]$ sudo firewall-cmd --reload
success
[nono@node1 conf.d]$ sudo systemctl restart nginx
```

🌞 Visitez votre super site web

```powershell
[nono@node1 conf.d]$ curl 10.2.1.11:26171
<h1>MEOW mon premier serveur web <33</h1>
```

## III. Your own services

### 1. Au cas où vous l'auriez oublié

### 2. Analyse des services existants

🌞 Afficher le fichier de service SSH

```
[nono@node1 ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; preset: enabled)
     Active: active (running) since Tue 2024-01-30 11:37:55 CET; 23min ago

[nono@node1 ~]$ cat /usr/lib/systemd/system/sshd.service | grep ExecStart=
ExecStart=/usr/sbin/sshd -D $OPTIONS

[nono@node1 ~]$ sudo /usr/sbin/sshd -D $OPTIONS
```

🌞 Afficher le fichier de service NGINX

```
[nono@node1 ~]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; preset: disabled)
     Active: active (running) since Tue 2024-01-30 12:10:39 CET; 8s ago

[nono@node1 ~]$ cat /usr/lib/systemd/system/nginx.service | grep ExecStart=
ExecStart=/usr/sbin/nginx
```

### 3. Création de service

🌞 Créez le fichier /etc/systemd/system/tp3_nc.service

```
[nono@node1 system]$ sudo cat tp3_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 16362 -k
```

🌞 Indiquer au système qu'on a modifié les fichiers de service

>sudo systemctl daemon-reload

🌞 Démarrer notre service de ouf

```
[nono@node1 system]$ sudo systemctl start tp3_nc
```

🌞 Vérifier que ça fonctionne

```
[nono@node1 system]$ sudo systemctl status tp3_nc.service
● tp3_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp3_nc.service; static)
     Active: active (running) since Tue 2024-01-30 11:23:09 CET; 50s ago
   Main PID: 22129 (nc)
      Tasks: 1 (limit: 4896)
     Memory: 1.1M
        CPU: 4ms
     CGroup: /system.slice/tp3_nc.service
             └─22129 /usr/bin/nc -l 16362 -k
```
```
[nono@node1 system]$ ss -tln | grep 16362
LISTEN 0      10           0.0.0.0:16362      0.0.0.0:*
LISTEN 0      10              [::]:16362         [::]:*
```

🌞 Les logs de votre service

```
[nono@node1 system]$ sudo journalctl -xe -u tp3_nc | grep start
░░ Subject: A start job for unit tp3_nc.service has finished successfully
░░ A start job for unit tp3_nc.service has finished successfully.
```
```
[nono@node1 system]$ sudo journalctl -xe -u tp3_nc | grep nc
░░ Subject: A start job for unit tp3_nc.service has finished successfully
░░ A start job for unit tp3_nc.service has finished successfully.
Jan 30 12:13:20 node1.tp3.b1 nc[22129]: bonjour

```
```
[nono@node1 system]$  sudo journalctl -xe -u tp3_nc | grep finished
░░ Subject: A start job for unit tp3_nc.service has finished successfully
░░ A start job for unit tp3_nc.service has finished successfully. 
```

🌞 S'amuser à kill le processus

```
[nono@node1 system]$ ps -ef | grep nc
root       22129       1  0 16:36 ?        00:00:00 /usr/bin/nc -l 16362 -k

[nono@node1 system]$ sudo kill 22129 
```

🌞 Affiner la définition du service

```
[nono@node1 system]$ sudo cat /etc/systemd/system/tp3_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
Restart=always
ExecStart=/usr/bin/nc -l 16362 -k
```
```
[nono@node1 system]$ sudo systemctl start tp3_nc.service
[nono@node1 system]$  ps -fe | grep nc
dbus         657       1  0 08:59 ?        00:00:00 /usr/bin/dbus-broker-launch --scope system --audit
root         693       1  0 08:59 ?        00:00:00 login -- nono   
nono         839       1  0 09:05 ?        00:00:00 /usr/lib/systemd/systemd --user
nono         841     839  0 09:05 ?        00:00:00 (sd-pam)
nono         849     693  0 09:05 tty1     00:00:00 -bash
root       22166   18011  0 12:01 ?        00:00:00 sshd: nono [priv]
nono       22170   22166  0 12:01 ?        00:00:00 sshd: nono@pts/0
nono       22171   22170  0 12:01 pts/0    00:00:00 -bash
root       22259       1  0 12:30 ?        00:00:00 /usr/bin/nc -l 16362 -k
nono       22260   22171  0 12:30 pts/0    00:00:00 ps -fe
nono       22261   22171  0 12:30 pts/0    00:00:00 grep --color=auto nc
```
```
[nono@node1 system]$ sudo kill 22259
```
```
[nono@node1 system]$  ps -fe | grep nc
dbus         657       1  0 08:59 ?        00:00:00 /usr/bin/dbus-broker-launch --scope system --audit
root         693       1  0 08:59 ?        00:00:00 login -- nono   
nono         839       1  0 09:05 ?        00:00:00 /usr/lib/systemd/systemd --user
nono         841     839  0 09:05 ?        00:00:00 (sd-pam)
nono         849     693  0 09:05 tty1     00:00:00 -bash
root       22166   18011  0 12:01 ?        00:00:00 sshd: nono [priv]
nono       22170   22166  0 12:01 ?        00:00:00 sshd: nono@pts/0
nono       22171   22170  0 12:01 pts/0    00:00:00 -bash
root       22265       1  0 12:30 ?        00:00:00 /usr/bin/nc -l 16362 -k
nono       22266   22171  0 12:30 pts/0    00:00:00 ps -fe
nono       22267   22171  0 12:30 pts/0    00:00:00 grep --color=auto nc
```

