# TP1 - Casser avant de construire 

## II. Casser

### 2. Fichier

---
🌞 Supprimer des fichiers

>su
>
>cd /
>
>cd boot/
>
>sudo rm init*

---
### 3. Utilisateurs

🌞 Mots de passe

```powershell
[root@localhost ~]# sudo awk -F: '$2 ~ /^[^!*]/ {print $1}' /etc/shadow
root
nono

[root@localhost ~]# users_with_password=$(sudo awk -F: '$2 ~ /^[^!*]/ {print $1}' /etc/shadow)

for user in $users_with_password; do
    sudo passwd $user
done
Changing password for user root.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
Sorry, passwords do not match.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.
Changing password for user nono.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.
```

---
🌞 Another way ?

```powershell
for user in $(cut -d: -f1 /etc/passwd); do
    sudo usermod --shell /usr/sbin/nologin $user done

[root@localhost ~]# grep '/usr/sbin/nologin' /etc/passwd
-> root:x:0:0:root:/root:/usr/sbin/nologin
bin:x:1:1:bin:/bin:/usr/sbin/nologin
daemon:x:2:2:daemon:/sbin:/usr/sbin/nologin
adm:x:3:4:adm:/var/adm:/usr/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/usr/sbin/nologin
sync:x:5:0:sync:/sbin:/usr/sbin/nologin
shutdown:x:6:0:shutdown:/sbin:/usr/sbin/nologin
halt:x:7:0:halt:/sbin:/usr/sbin/nologin
mail:x:8:12:mail:/var/spool/mail:/usr/sbin/nologin
operator:x:11:0:operator:/root:/usr/sbin/nologin
games:x:12:100:games:/usr/games:/usr/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/usr/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/usr/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/usr/sbin/nologin
dbus:x:81:81:System message bus:/:/usr/sbin/nologin
tss:x:59:59:Account used for TPM access:/dev/null:/usr/sbin/nologin
sssd:x:998:995:User for sssd:/:/usr/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/usr/share/empty.sshd:/usr/sbin/nologin
chrony:x:997:994:chrony system user:/var/lib/chrony:/usr/sbin/nologin
systemd-oom:x:992:992:systemd Userspace OOM Killer:/:/usr/sbin/nologin
-> nono:x:1000:1000:nono:/home/nono:/usr/sbin/nologin
tcpdump:x:72:72::/:/usr/sbin/nologin
```

```powershell
[nono@localhost ~]$ su
Password:
This account is currently not available.
[nono@localhost ~]$
```

## 4. Disques

🌞 Effacer le contenu du disque dur

```powershell
[nono@localhost ~]$ sudo dd if=/dev/zero of=/dev/sdX bs=4M status=progress
dd: error writing '/dev/sdX': No space left on device
111+0 records in
110+0 records out
4194304 bytes (481 MB, 460 MiB) copied, 0.064362 s, 142 MB/s
-------------------------------
[nono@localhost ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             460M  460M     0 100% /dev
tmpfs                386M     0  386M   0% /dev/shm
tmpfs                155M  3.6M  155M   3% /run
/dev/mapper/rl-root   17G  1.3G   16G   8% /
/dev/sda1           1014M  220M  745M  22% /boot
tmpfs                 78M     0   78M   0% /run/user/1000
------------------------------
[nono@localhost ~]$ sudo dd if=/dev/zero of=/dev/mapper/rl-root bs=4M status=progress
18111004672 bytes (18 GB, 17 GiB) copied, 91 s, 199 MB/s
dd: error writing '/dev/mapper/rl-root': No space left on device
4352+0 records in
4351+0 records out
18249416704 bytes (18 GB, 17 GiB) copied, 91.6934 s, 199 MB/s
Segmentation fault
```

## 5. Malware

🌞 Reboot automatique

```powershell
[nono@localhost ~]$ nano ~/.bashrc

# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
        for rc in ~/.bashrc.d/*; do
                if [ -f "$rc" ]; then
                        . "$rc"
                fi
        done
fi

unset rc

sudo reboot
```

## 6. You own way

🌞 Trouvez 4 autres façons de détuire la machine

1) Le fork bomb : surcharge des taches de la vm

    :(){ :|:& };: 

2) En autorisant que 1 processus maximum

> sudo nano /etc/security/limits.conf 

```powershell
#<domain>       <type>  <item>      <value>
#

*               soft    core        0
*               hard    nproc       1

```

> sudo systemctl restart NetworkManager

Quand on essaie de rallumer la vm et de se connecter, une erreur bash apparait comme quoi il peut pas y avoir plus de 1 processus en cours.

3) 

4) 