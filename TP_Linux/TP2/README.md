# TP2 : Appréhension d'un système Linux

## Partie 1 : Files and users

### I. Fichiers
---
#### 1. Find me
---
🌞 Trouver le chemin vers le répertoire personnel de votre utilisateur

```
[nono@vm ~]$ pwd
/home/nono
```

🌞 Trouver le chemin du fichier de logs SSH

```
/var/log/secure

[nono@vm log]$ sudo tail cat secure
tail: cannot open 'cat' for reading: No such file or directory
==> secure <==
Jan 22 10:20:33 vm sshd[682]: Server listening on 0.0.0.0 port 22.
Jan 22 10:20:33 vm sshd[682]: Server listening on :: port 22.
Jan 22 10:20:41 vm systemd[1268]: pam_unix(systemd-user:session): session opened for user nono(uid=1000) by (uid=0)
Jan 22 10:20:41 vm login[689]: pam_unix(login:session): session opened for user nono(uid=1000) by LOGIN(uid=0)
Jan 22 10:20:41 vm login[689]: LOGIN ON tty1 BY nono
Jan 22 10:21:47 vm sshd[1302]: Accepted password for nono from 10.2.1.1 port 62646 ssh2
Jan 22 10:21:47 vm sshd[1302]: pam_unix(sshd:session): session opened for user nono(uid=1000) by (uid=0)
Jan 22 10:39:37 vm sudo[1350]:    nono : TTY=pts/0 ; PWD=/var/log ; USER=root ; COMMAND=/bin/cat secure
Jan 22 10:39:37 vm sudo[1350]: pam_unix(sudo:session): session opened for user root(uid=0) by nono(uid=1000)
Jan 22 10:39:37 vm sudo[1350]: pam_unix(sudo:session): session closed for user root
```

🌞 Trouver le chemin du fichier de configuration du serveur SSH

```
/etc/ssh/sshd_config
```

### II. Users
---
#### 1. Nouveau user
---

🌞 Créer un nouvel utilisateur

```
sudo useradd marmotte -d /home/papier_alu/

sudo passwd marmotte
```

#### 2. Infos enregistrées par le système
---
🌞 Prouver que cet utilisateur a été créé

```
[marmotte@vm ~]$ cat /etc/passwd | grep marmotte
marmotte:x:1001:1001::/home/papier_alu/:/bin/bash
```

🌞 Déterminer le hash du password de l'utilisateur marmotte

```
[nono@vm ~]$ sudo cat /etc/shadow | grep marmotte
marmotte:$6$P/LQYjT3UF8uNfP1$nm9HELGbPehlcwNVQt7.8vJbAYBPMrFhQ0i/uCe7h.lwXX5w/UPDpVICvHLE939Vue41fGKwDplz5STJtQL0m/:19744:0:99999:7:::
```

#### 3. Connexion sur le nouvel utilisateur
---

🌞 Tapez une commande pour vous déconnecter : fermer votre session utilisateur

```
exit
```

🌞 Assurez-vous que vous pouvez vous connecter en tant que l'utilisateur marmotte

```
[nono@vm ~]$ su marmotte
Password:
[marmotte@vm nono]$ ls
ls: cannot open directory '.': Permission denied
[marmotte@vm nono]$
```

## Partie 2 : Programmes et paquets

### I. Programmes et processus
---
#### 1. Run then kill
---

🌞 Lancer un processus sleep

```powershell
[nono@vm ~]$ sleep 1000

[nono@vm ~]$ ps -u | grep sleep
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
nono        1869  0.0  0.1   5584  1016 pts/0    S+   11:52   0:00 sleep 1000
```

🌞 Terminez le processus sleep depuis le deuxième terminal

```
[nono@vm ~]$ kill 1869
```

#### 2. Tâche de fond
---
🌞 Lancer un nouveau processus sleep, mais en tâche de fond

```
[nono@vm ~]$ sleep 1000&
[2] 1885
```

🌞 Visualisez la commande en tâche de fond

```
[nono@vm ~]$ jobs
[2]+  Running                 sleep 1000 &
```

#### 3. Find paths
---
🌞 Trouver le chemin où est stocké le programme sleep

```
[nono@vm ~]$ which sleep
/usr/bin/sleep

[nono@vm ~]$ ls -al /usr/bin/sleep | grep sleep
-rwxr-xr-x. 1 root root 36312 Apr 24  2023 /usr/bin/sleep
```

🌞 Tant qu'on est à chercher des chemins : trouver les chemins vers tous les fichiers qui s'appellent .bashrc

```
[nono@vm ~]$ sudo find / -name "*.bashrc"
[sudo] password for nono:
/etc/skel/.bashrc
/root/.bashrc
/home/nono/.bashrc
/home/papier_alu/.bashrc
```

#### 4. La variable PATH
---
🌞 Vérifier que

```powershell
[nono@vm ~]$ echo $PATH
/home/nono/.local/bin:/home/nono/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin
```

```powershell
[nono@vm ~]$ which sleep
/usr/bin/sleep
```

```powershell
[nono@vm ~]$ which ssh
/usr/bin/ssh
```

```powershell
[nono@vm ~]$ which ping
/usr/bin/ping
```

### II. Paquets
---

🌞 Installer le paquet git

> sudo dnf install git

🌞 Utiliser une commande pour lancer git

```
[nono@vm ~]$ which git
/usr/bin/git
```

🌞 Installer le paquet nginx

> sudo dnf install nginx

🌞 Déterminer

```
[nono@vm ~]$ which nginx
/usr/sbin/nginx

[nono@vm ~]$ sudo find / -name "*nginx.conf"
/etc/nginx/nginx.conf
```

🌞 Mais aussi déterminer.

```
[nono@vm ~]$ cd /etc/yum.repos.d

[nono@vm yum.repos.d]$ grep -nri  -E '^mirror'
rocky-addons.repo:13:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=HighAvailability-$releasever$rltype
rocky-addons.repo:23:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=HighAvailability-$releasever-debug$rltype
rocky-addons.repo:32:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=source&repo=HighAvailability-$releasever-source$rltype
rocky-addons.repo:41:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=ResilientStorage-$releasever$rltype
rocky-addons.repo:51:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=ResilientStorage-$releasever-debug$rltype
rocky-addons.repo:60:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=source&repo=ResilientStorage-$releasever-source$rltype
rocky-addons.repo:69:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=NFV-$releasever$rltype
rocky-addons.repo:79:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=RT-$releasever-debug$rltype
rocky-addons.repo:88:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=RT-$releasever-source$rltype
rocky-addons.repo:97:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=RT-$releasever$rltype
rocky-addons.repo:107:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=RT-$releasever-debug$rltype
rocky-addons.repo:116:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=RT-$releasever-source$rltype
rocky-addons.repo:125:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=SAP-$releasever$rltype
rocky-addons.repo:135:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=SAP-$releasever-debug$rltype
rocky-addons.repo:144:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=SAP-$releasever-source$rltype
rocky-addons.repo:153:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=SAPHANA-$releasever$rltype
rocky-addons.repo:163:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=SAPHANA-$releasever-debug$rltype
rocky-addons.repo:172:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=SAPHANA-$releasever-source$rltype
rocky-devel.repo:7:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=devel-$releasever$rltype
rocky-devel.repo:16:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=devel-$releasever-debug$rltype
rocky-devel.repo:25:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=devel-$releasever-source$rltype
rocky-extras.repo:13:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=extras-$releasever$rltype
rocky-extras.repo:23:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=extras-$releasever-debug$rltype
rocky-extras.repo:32:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=extras-$releasever-source$rltype
rocky-extras.repo:41:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=plus-$releasever$rltype
rocky-extras.repo:51:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=plus-$releasever-debug$rltype
rocky-extras.repo:60:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=source&repo=plus-$releasever-source$rltype
rocky.repo:13:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=BaseOS-$releasever$rltype
rocky.repo:23:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=BaseOS-$releasever-debug$rltype
rocky.repo:32:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=source&repo=BaseOS-$releasever-source$rltype
rocky.repo:41:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=AppStream-$releasever$rltype
rocky.repo:51:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=AppStream-$releasever-debug$rltype
rocky.repo:60:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=source&repo=AppStream-$releasever-source$rltype
rocky.repo:69:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=CRB-$releasever$rltype
rocky.repo:79:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=CRB-$releasever-debug$rltype
rocky.repo:88:mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=source&repo=CRB-$releasever-source$rltype
```

## Partie 3 : Poupée russe

🌞 Récupérer le fichier meow

```
[nono@vm ~]$ sudo wget https://gitlab.com/it4lik/b1-linux-2023/-/blob/master/tp/2/meow

[nono@vm ~]$ ls
meow
```

🌞 Trouver le dossier dawa/

```
[nono@vm 2]$ file meow
meow: Zip archive data, at least v2.0 to extract
```

```
[nono@vm ~]$ sudo mv meow meow.zip

[nono@vm 2]$ unzip meow.zip
Archive:  meow.zip
  inflating: meow
```

```
[nono@vm ~]$ sudo mv meow meow.xz

[nono@vm 2]$ file meow.xz
meow.xz: XZ compressed data

[nono@vm 2]$ xz -d -F xz meow.xz

[nono@vm 2]$ file meow
meow: bzip2 compressed data, block size = 900k

[nono@vm 2]$ bzip2 -d meow
bzip2: Can't guess original name for meow -- using meow.out

[nono@vm 2]$ file meow.out
meow.out: RAR archive data, v5

[nono@vm 2]$ unrar e meow.out

[nono@vm 2]$ file meow
meow: gzip compressed data, from Unix, original size modulo 2^32 145049600


```

🌞 Dans le dossier dawa/, déterminer le chemin vers

le seul fichier de 15Mo :
```
[nono@vm 2]$ find dawa/ -size 15M
dawa/folder31/19/file39
```

le seul fichier qui ne contient que des 7 :
```
[nono@vm 2]$ grep -r 7777777
folder43/38/file41:77777777777
```
le seul fichier qui est nommé cookie :

```
[nono@vm 2]$ find -name cookie
./folder14/25/cookie
```

le seul fichier caché (un fichier caché c'est juste un fichier dont le nom commence par un .) :
```
[nono@vm 2]$ find -name ".*"
.
./folder32/14/.hidden_file
```

le seul fichier qui date de 2014:
```
[nono@vm 2]$ find -newermt '2014-01-01 23:00' -not -newermt '2015-01-01 23:00'
./folder36/40/file43
```

le seul fichier qui a 5 dossiers-parents :
```
[nono@vm 2]$ find -wholename "*/*/*/*/*/*/*"
./folder37/45/23/43/54/file43
```