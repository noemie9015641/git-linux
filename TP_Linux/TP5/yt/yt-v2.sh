#!/bin/bash

download="/srv/yt/downloads"
if [ ! -d "${download}" ]; then
    echo "The folder downloads doesn't exist."
    exit 1
fi

log="/var/log/yt"
if [ ! -d "${log}" ]; then
    echo "The folder log doesn't exist."
    exit 1
fi

url_file="/srv/yt/url.txt"

process_url() {
    local url=$1
    if [ -z "$url" ]; then
        echo "File empty."
        return
    fi

    name=$(youtube-dl --get-filename -o "%(title)s" "${url}")
    video_dir="${download}/${name}"
    mkdir -p "${video_dir}"

    youtube-dl -o "${video_dir}/${name}.mp4" "${url}" > /dev/null
    youtube-dl --write-description -o "$video_dir/description.txt" "${url}" > /dev/null

    date=$(date "+%y/%m/%d %T")
    echo "[${date}] Video ${url} was downloaded. File path : srv/yt/${video_dir}/${name}.mp4" >> "${log}/download.log"

    echo "Video ${url} was downloaded."
    echo "File path : srv/yt/${video_dir}/${name}.mp4"

    sed -i '1d' "$url_file"
}

while true; do
    if [ -s "$url_file" ]; then
        while IFS= read -r url || [[ -n "$url" ]]; do
            process_url "$url"
        done < "$url_file"
    else
        echo "The file of URL is empty or doesn't exist."
    fi
    sleep 10
done