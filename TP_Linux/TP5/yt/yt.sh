download="downloads"
if [ ! -d "$download" ]; then
    echo "The folder downloads doesnt exist."
    exit 1
fi

log="/var/log/yt"
if [ ! -d "$log" ]; then
    echo "The folder log doesnt exist."
    exit 1
fi

url="$1"
if [ -z "$url" ]; then
    echo "URL :"
    exit 1
fi

name=$(youtube-dl --get-filename -o "%(title)s" "$url")
video_dir="$download/$name"
mkdir -p "$video_dir"

youtube-dl -o "$video_dir/$name.mp4" "$url" > /dev/null
youtube-dl --write-description -o "$video_dir/description.txt" "$url" > /dev/null

date=$(date "+%y/%m/%d %T")
echo "[$date] Video $url was downloaded. File path : srv/yt/${video_dir}/${name}.mp4" >> "$log/download.log"


echo "Video ${url} was downloaded."
echo "File path : srv/yt/${video_dir}/${name}.mp4"
